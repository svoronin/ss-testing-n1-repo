// Setting up the project
project --topLevelPackage com.softserve.fproject
persistence setup --provider HIBERNATE --database MYSQL --databaseName SSFProject --userName ssfpuser --password ssfpsecurity
//persistence setup --provider HIBERNATE --database HYPERSONIC_IN_MEMORY

// Cashier domain
entity --class ~.domain.Cashier --testAutomatically
field string --fieldName login --sizeMin 6 --sizeMax 32 --unique --notNull
field string --fieldName password --sizeMin 32 --sizeMax 32 --notNull

// Country domain
entity --class ~.domain.Country --testAutomatically 
field string --fieldName name --sizeMax 30 --notNull

// City domain 
entity --class ~.domain.City --testAutomatically 
field string --fieldName name --sizeMax 30 --notNull
field reference --fieldName country --type ~.domain.Country --notNull

// Airport domain
entity --class ~.domain.Airport --testAutomatically
field string --fieldName name --sizeMax 30 --notNull
field reference --fieldName city --type ~.domain.City --notNull

// #NEW! Terminal classes domain
enum type --class ~.domain.TerminalClass
enum constant --name A 
enum constant --name B
enum constant --name C
enum constant --name D
enum constant --name E
enum constant --name F 

// #NEW! Terminal domain
entity --class ~.domain.Terminal --testAutomatically
field enum --fieldName terminalName --type ~.domain.TerminalClass --notNull
field reference --fieldName airport --type ~.domain.Airport --notNull

// Hop domain - parts of whole flight
entity --class  ~.domain.Hop --testAutomatically
field reference --fieldName fromAirport --type ~.domain.Airport --notNull
field reference --fieldName toAirport --type ~.domain.Airport --notNull
field number --fieldName duration --type java.lang.Float --digitsInteger 2 --digitsFraction 1 --notNull

// Plane domain
entity --class ~.domain.Plane --testAutomatically
field reference --fieldName planeModel --type ~.domain.PlaneModel
field string --fieldName name --sizeMax 40 --notNull
field number --fieldName firstClassSitsCount --type java.lang.Integer --notNull --min 0
field number --fieldName secondClassSitsCount --type java.lang.Integer --notNull --min 0

// PlannedFlight domain
entity --class  ~.domain.PlannedFlight --testAutomatically
field set --fieldName hops --type ~.domain.Hop --notNull
field reference --fieldName plane --type ~.domain.Plane --notNull
field number --fieldName firstClassSitsCost --type java.lang.Double --notNull --digitsInteger 8 --digitsFraction 2
field number --fieldName secondClassSitsCost --type java.lang.Double --notNull --digitsInteger 8 --digitsFraction 2

// Periodicity domain
entity --class ~.domain.Periodicity --testAutomatically
field date --fieldName startingDate --type java.util.Date --notNull
field number --fieldName period --type java.lang.Integer --notNull --min 0 --max 31
field number --fieldName offset --type java.lang.Integer --notNull --min 0 --max 31
field reference --fieldName flight --type ~.domain.PlannedFlight

// Flight states domain
enum type --class ~.domain.FlightState
enum constant --name Arrived
enum constant --name RegistrationIsOpenned
enum constant --name RegistrationIsClosed
enum constant --name AlreadyLeft
enum constant --name Cancelled

// Flight domain
entity --class ~.domain.Flight --testAutomatically
field date --fieldName onDate --type java.util.Date --notNull
field set --fieldName hops --type ~.domain.Hop --notNull
field reference --fieldName plane --type ~.domain.Plane --notNull
field number --fieldName firstClassSitsCost --type java.lang.Double --notNull --digitsInteger 8 --digitsFraction 2
field number --fieldName secondClassSitsCost --type java.lang.Double --notNull --digitsInteger 8 --digitsFraction 2
field number --fieldName firstClassVacancies --type java.lang.Integer --notNull --min 0
field number --fieldName secondClassVacancies --type java.lang.Integer --notNull --min 0
field enum --fieldName status --type ~.domain.FlightState

// #NEW! Plane service domain


// Client domain
entity --class ~.domain.Client --testAutomatically
field string --fieldName firstName --sizeMax 60 --notNull
field string  --fieldName lastName --sizeMax 60 --notNull
field string --fieldName middleName --sizeMax 60 --notNull
field string --fieldName phone --sizeMax 14
field string --fieldName passport --sizeMax 10 --notNull
field string --fieldName email --sizeMax 60

// Ticket domain
entity --class ~.domain.Ticket --testAutomatically 
field number --fieldName price --type java.lang.Double --notNull --digitsInteger 8 --digitsFraction 2
field number --fieldName discount --type java.lang.Double --notNull --digitsInteger 8 --digitsFraction 2
field number --fieldName sitClass --type java.lang.Byte --min 1 --max 3 --value 3
field boolean --fieldName isRegistred --notNull
field boolean --fieldName isOnlyReserved --notNull
field reference --fieldName client --type ~.domain.Client --notNull
field reference --fieldName flight --type ~.domain.Flight --notNull

// controller all --package ~.web
web mvc setup
controller class --class ~.cashier.Authorization
controller class --class ~.web_client.WelcomeScreen
class --class ~.serviceWorker.TasksList
class --class ~.common.mvc.FlightList
class --class ~.flightManager.FlightEditor
class --class ~.flightRegistrator.PassangerList
class --class ~.boardRegistrator.PassangerList

security setup

// Finalization
perform tests
perform package
quit
