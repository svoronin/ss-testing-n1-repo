// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.softserve.fproject.domain;

import com.softserve.fproject.domain.AirportDataOnDemand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

privileged aspect AirportIntegrationTest_Roo_IntegrationTest {
    
    declare @type: AirportIntegrationTest: @RunWith(SpringJUnit4ClassRunner.class);
    
    declare @type: AirportIntegrationTest: @ContextConfiguration(locations = "classpath:/META-INF/spring/applicationContext.xml");
    
    declare @type: AirportIntegrationTest: @Transactional;
    
    @Autowired
    private AirportDataOnDemand AirportIntegrationTest.dod;
    
    @Test
    public void AirportIntegrationTest.testCountAirports() {
        org.junit.Assert.assertNotNull("Data on demand for 'Airport' failed to initialize correctly", dod.getRandomAirport());
        long count = com.softserve.fproject.domain.Airport.countAirports();
        org.junit.Assert.assertTrue("Counter for 'Airport' incorrectly reported there were no entries", count > 0);
    }
    
    @Test
    public void AirportIntegrationTest.testFindAirport() {
        com.softserve.fproject.domain.Airport obj = dod.getRandomAirport();
        org.junit.Assert.assertNotNull("Data on demand for 'Airport' failed to initialize correctly", obj);
        java.lang.Long id = obj.getId();
        org.junit.Assert.assertNotNull("Data on demand for 'Airport' failed to provide an identifier", id);
        obj = com.softserve.fproject.domain.Airport.findAirport(id);
        org.junit.Assert.assertNotNull("Find method for 'Airport' illegally returned null for id '" + id + "'", obj);
        org.junit.Assert.assertEquals("Find method for 'Airport' returned the incorrect identifier", id, obj.getId());
    }
    
    @Test
    public void AirportIntegrationTest.testFindAllAirports() {
        org.junit.Assert.assertNotNull("Data on demand for 'Airport' failed to initialize correctly", dod.getRandomAirport());
        long count = com.softserve.fproject.domain.Airport.countAirports();
        org.junit.Assert.assertTrue("Too expensive to perform a find all test for 'Airport', as there are " + count + " entries; set the findAllMaximum to exceed this value or set findAll=false on the integration test annotation to disable the test", count < 250);
        java.util.List<com.softserve.fproject.domain.Airport> result = com.softserve.fproject.domain.Airport.findAllAirports();
        org.junit.Assert.assertNotNull("Find all method for 'Airport' illegally returned null", result);
        org.junit.Assert.assertTrue("Find all method for 'Airport' failed to return any data", result.size() > 0);
    }
    
    @Test
    public void AirportIntegrationTest.testFindAirportEntries() {
        org.junit.Assert.assertNotNull("Data on demand for 'Airport' failed to initialize correctly", dod.getRandomAirport());
        long count = com.softserve.fproject.domain.Airport.countAirports();
        if (count > 20) count = 20;
        java.util.List<com.softserve.fproject.domain.Airport> result = com.softserve.fproject.domain.Airport.findAirportEntries(0, (int) count);
        org.junit.Assert.assertNotNull("Find entries method for 'Airport' illegally returned null", result);
        org.junit.Assert.assertEquals("Find entries method for 'Airport' returned an incorrect number of entries", count, result.size());
    }
    
    @Test
    public void AirportIntegrationTest.testFlush() {
        com.softserve.fproject.domain.Airport obj = dod.getRandomAirport();
        org.junit.Assert.assertNotNull("Data on demand for 'Airport' failed to initialize correctly", obj);
        java.lang.Long id = obj.getId();
        org.junit.Assert.assertNotNull("Data on demand for 'Airport' failed to provide an identifier", id);
        obj = com.softserve.fproject.domain.Airport.findAirport(id);
        org.junit.Assert.assertNotNull("Find method for 'Airport' illegally returned null for id '" + id + "'", obj);
        boolean modified =  dod.modifyAirport(obj);
        java.lang.Integer currentVersion = obj.getVersion();
        obj.flush();
        org.junit.Assert.assertTrue("Version for 'Airport' failed to increment on flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }
    
    @Test
    public void AirportIntegrationTest.testMerge() {
        com.softserve.fproject.domain.Airport obj = dod.getRandomAirport();
        org.junit.Assert.assertNotNull("Data on demand for 'Airport' failed to initialize correctly", obj);
        java.lang.Long id = obj.getId();
        org.junit.Assert.assertNotNull("Data on demand for 'Airport' failed to provide an identifier", id);
        obj = com.softserve.fproject.domain.Airport.findAirport(id);
        boolean modified =  dod.modifyAirport(obj);
        java.lang.Integer currentVersion = obj.getVersion();
        com.softserve.fproject.domain.Airport merged = (com.softserve.fproject.domain.Airport) obj.merge();
        obj.flush();
        org.junit.Assert.assertEquals("Identifier of merged object not the same as identifier of original object", merged.getId(), id);
        org.junit.Assert.assertTrue("Version for 'Airport' failed to increment on merge and flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }
    
    @Test
    public void AirportIntegrationTest.testPersist() {
        org.junit.Assert.assertNotNull("Data on demand for 'Airport' failed to initialize correctly", dod.getRandomAirport());
        com.softserve.fproject.domain.Airport obj = dod.getNewTransientAirport(Integer.MAX_VALUE);
        org.junit.Assert.assertNotNull("Data on demand for 'Airport' failed to provide a new transient entity", obj);
        org.junit.Assert.assertNull("Expected 'Airport' identifier to be null", obj.getId());
        obj.persist();
        obj.flush();
        org.junit.Assert.assertNotNull("Expected 'Airport' identifier to no longer be null", obj.getId());
    }
    
    @Test
    public void AirportIntegrationTest.testRemove() {
        com.softserve.fproject.domain.Airport obj = dod.getRandomAirport();
        org.junit.Assert.assertNotNull("Data on demand for 'Airport' failed to initialize correctly", obj);
        java.lang.Long id = obj.getId();
        org.junit.Assert.assertNotNull("Data on demand for 'Airport' failed to provide an identifier", id);
        obj = com.softserve.fproject.domain.Airport.findAirport(id);
        obj.remove();
        obj.flush();
        org.junit.Assert.assertNull("Failed to remove 'Airport' with identifier '" + id + "'", com.softserve.fproject.domain.Airport.findAirport(id));
    }
    
}
