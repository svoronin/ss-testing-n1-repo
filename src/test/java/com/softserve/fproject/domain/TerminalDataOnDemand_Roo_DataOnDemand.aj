// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.softserve.fproject.domain;

import com.softserve.fproject.domain.AirportDataOnDemand;
import com.softserve.fproject.domain.Terminal;
import java.util.List;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

privileged aspect TerminalDataOnDemand_Roo_DataOnDemand {
    
    declare @type: TerminalDataOnDemand: @Component;
    
    private Random TerminalDataOnDemand.rnd = new java.security.SecureRandom();
    
    private List<Terminal> TerminalDataOnDemand.data;
    
    @Autowired
    private AirportDataOnDemand TerminalDataOnDemand.airportDataOnDemand;
    
    public Terminal TerminalDataOnDemand.getNewTransientTerminal(int index) {
        com.softserve.fproject.domain.Terminal obj = new com.softserve.fproject.domain.Terminal();
        setTerminalName(obj, index);
        setAirport(obj, index);
        return obj;
    }
    
    private void TerminalDataOnDemand.setTerminalName(Terminal obj, int index) {
        com.softserve.fproject.domain.TerminalClass terminalName = com.softserve.fproject.domain.TerminalClass.class.getEnumConstants()[0];
        obj.setTerminalName(terminalName);
    }
    
    private void TerminalDataOnDemand.setAirport(Terminal obj, int index) {
        com.softserve.fproject.domain.Airport airport = airportDataOnDemand.getRandomAirport();
        obj.setAirport(airport);
    }
    
    public Terminal TerminalDataOnDemand.getSpecificTerminal(int index) {
        init();
        if (index < 0) index = 0;
        if (index > (data.size() - 1)) index = data.size() - 1;
        Terminal obj = data.get(index);
        return Terminal.findTerminal(obj.getId());
    }
    
    public Terminal TerminalDataOnDemand.getRandomTerminal() {
        init();
        Terminal obj = data.get(rnd.nextInt(data.size()));
        return Terminal.findTerminal(obj.getId());
    }
    
    public boolean TerminalDataOnDemand.modifyTerminal(Terminal obj) {
        return false;
    }
    
    public void TerminalDataOnDemand.init() {
        data = com.softserve.fproject.domain.Terminal.findTerminalEntries(0, 10);
        if (data == null) throw new IllegalStateException("Find entries implementation for 'Terminal' illegally returned null");
        if (!data.isEmpty()) {
            return;
        }
        
        data = new java.util.ArrayList<com.softserve.fproject.domain.Terminal>();
        for (int i = 0; i < 10; i++) {
            com.softserve.fproject.domain.Terminal obj = getNewTransientTerminal(i);
            obj.persist();
            obj.flush();
            data.add(obj);
        }
    }
    
}
