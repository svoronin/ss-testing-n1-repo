package com.softserve.fproject.domain;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import com.softserve.fproject.domain.TerminalClass;
import javax.validation.constraints.NotNull;
import javax.persistence.Enumerated;
import com.softserve.fproject.domain.Airport;
import javax.persistence.ManyToOne;

@RooJavaBean
@RooToString
@RooEntity
public class Terminal {

    @NotNull
    @Enumerated
    private TerminalClass terminalName;

    @NotNull
    @ManyToOne
    private Airport airport;
}
