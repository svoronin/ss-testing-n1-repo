package com.softserve.fproject.domain;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.Min;
import javax.validation.constraints.Max;
import com.softserve.fproject.domain.PlannedFlight;
import javax.persistence.ManyToOne;

@RooJavaBean
@RooToString
@RooEntity
public class Periodicity {

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "S-")
    private Date startingDate;

    @NotNull
    @Min(0L)
    @Max(31L)
    private Integer period;

    @NotNull
    @Min(0L)
    @Max(31L)
    private Integer offset;

    @ManyToOne
    private PlannedFlight flight;
}
