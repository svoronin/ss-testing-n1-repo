package com.softserve.fproject.domain;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;
import javax.persistence.Column;
import javax.validation.constraints.Size;

@RooJavaBean
@RooToString
@RooEntity
public class Cashier {

    @NotNull
    @Column(unique = true)
    @Size(min = 6, max = 32)
    private String login;

    @NotNull
    @Size(min = 32, max = 32)
    private String password;
}
