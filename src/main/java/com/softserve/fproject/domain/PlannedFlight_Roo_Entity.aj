// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.softserve.fproject.domain;

import com.softserve.fproject.domain.PlannedFlight;
import java.lang.Integer;
import java.lang.Long;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Version;
import org.springframework.transaction.annotation.Transactional;

privileged aspect PlannedFlight_Roo_Entity {
    
    declare @type: PlannedFlight: @Entity;
    
    @PersistenceContext
    transient EntityManager PlannedFlight.entityManager;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long PlannedFlight.id;
    
    @Version
    @Column(name = "version")
    private Integer PlannedFlight.version;
    
    public Long PlannedFlight.getId() {
        return this.id;
    }
    
    public void PlannedFlight.setId(Long id) {
        this.id = id;
    }
    
    public Integer PlannedFlight.getVersion() {
        return this.version;
    }
    
    public void PlannedFlight.setVersion(Integer version) {
        this.version = version;
    }
    
    @Transactional
    public void PlannedFlight.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void PlannedFlight.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            PlannedFlight attached = PlannedFlight.findPlannedFlight(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void PlannedFlight.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void PlannedFlight.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public PlannedFlight PlannedFlight.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        PlannedFlight merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager PlannedFlight.entityManager() {
        EntityManager em = new PlannedFlight().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long PlannedFlight.countPlannedFlights() {
        return entityManager().createQuery("SELECT COUNT(o) FROM PlannedFlight o", Long.class).getSingleResult();
    }
    
    public static List<PlannedFlight> PlannedFlight.findAllPlannedFlights() {
        return entityManager().createQuery("SELECT o FROM PlannedFlight o", PlannedFlight.class).getResultList();
    }
    
    public static PlannedFlight PlannedFlight.findPlannedFlight(Long id) {
        if (id == null) return null;
        return entityManager().find(PlannedFlight.class, id);
    }
    
    public static List<PlannedFlight> PlannedFlight.findPlannedFlightEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM PlannedFlight o", PlannedFlight.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
