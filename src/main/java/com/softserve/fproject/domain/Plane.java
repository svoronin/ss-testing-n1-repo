package com.softserve.fproject.domain;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.constraints.Min;

@RooJavaBean
@RooToString
@RooEntity
public class Plane {

    @NotNull
    @Size(max = 40)
    private String name;

    @NotNull
    @Min(0L)
    private Integer firstClassSitsCount;

    @NotNull
    @Min(0L)
    private Integer secondClassSitsCount;
}
