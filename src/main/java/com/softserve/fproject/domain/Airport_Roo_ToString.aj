// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.softserve.fproject.domain;

import java.lang.String;

privileged aspect Airport_Roo_ToString {
    
    public String Airport.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("City: ").append(getCity()).append(", ");
        sb.append("Name: ").append(getName());
        return sb.toString();
    }
    
}
