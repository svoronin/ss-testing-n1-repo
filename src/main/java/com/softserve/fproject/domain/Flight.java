package com.softserve.fproject.domain;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Set;
import com.softserve.fproject.domain.Hop;
import java.util.HashSet;
import javax.persistence.ManyToMany;
import javax.persistence.CascadeType;
import com.softserve.fproject.domain.Plane;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import com.softserve.fproject.domain.FlightState;
import javax.persistence.Enumerated;

@RooJavaBean
@RooToString
@RooEntity
public class Flight {

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "S-")
    private Date onDate;

    @NotNull
    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Hop> hops = new HashSet<Hop>();

    @NotNull
    @ManyToOne
    private Plane plane;

    @NotNull
    @Digits(integer = 8, fraction = 2)
    private Double firstClassSitsCost;

    @NotNull
    @Digits(integer = 8, fraction = 2)
    private Double secondClassSitsCost;

    @NotNull
    @Min(0L)
    private Integer firstClassVacancies;

    @NotNull
    @Min(0L)
    private Integer secondClassVacancies;

    @Enumerated
    private FlightState status;
}
