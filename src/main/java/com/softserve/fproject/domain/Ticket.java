package com.softserve.fproject.domain;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Digits;
import org.springframework.beans.factory.annotation.Value;
import javax.validation.constraints.Min;
import javax.validation.constraints.Max;
import com.softserve.fproject.domain.Client;
import javax.persistence.ManyToOne;
import com.softserve.fproject.domain.Flight;

@RooJavaBean
@RooToString
@RooEntity
public class Ticket {

    @NotNull
    @Digits(integer = 8, fraction = 2)
    private Double price;

    @NotNull
    @Digits(integer = 8, fraction = 2)
    private Double discount;

    @Value("3")
    @Min(1L)
    @Max(3L)
    private Byte sitClass;

    @NotNull
    private Boolean isRegistred;

    @NotNull
    private Boolean isOnlyReserved;

    @NotNull
    @ManyToOne
    private Client client;

    @NotNull
    @ManyToOne
    private Flight flight;
}
