// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.softserve.fproject.domain;

import java.lang.String;

privileged aspect PlannedFlight_Roo_ToString {
    
    public String PlannedFlight.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("FirstClassSitsCost: ").append(getFirstClassSitsCost()).append(", ");
        sb.append("Hops: ").append(getHops() == null ? "null" : getHops().size()).append(", ");
        sb.append("Plane: ").append(getPlane()).append(", ");
        sb.append("SecondClassSitsCost: ").append(getSecondClassSitsCost());
        return sb.toString();
    }
    
}
